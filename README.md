Ruby Inform Port and Multi-user Server
======================================

This software is a Ruby port of the Inform parser, verb library, stock English
language responses library, and grammar specification sheet; as well as telnet
and websocket servers for running MUD-style games.

It includes many of the basic built-ins that you would expect to find
available when coding the routines of a typical Inform interactive-fiction
game code *.inf file.

This project uses the following external libraries and technologies:

* JRuby - Ruby programming language running on the JVM
* JBoss Netty - asynchronous, non-blocking IO, network driver
* Sequel - Ruby-based SQL database access toolkit and ORM
* PostgreSQL - Database for world-tree persistence
* java.util.concurrent - abstracted Thread management

The following features are provided:

* Command parser based on the venerable Inform 6 Parser
* Grammar parser supporting Inform 6 grammar definition files
* Multi-player networking support
* Java POSIX-compliant thread-based eventing
* Persistent world-tree
* Simple data model composed of these elements: object, tag, link, module
* Simple user account support
* Web client using websockets


## Why Ruby?

The correlation between Ruby programming idioms and Inform programming idioms
was surprisingly high compared to other programming languages, which made Ruby
a satisfyingly good choice for this project, in my opinion.

If you can write code in Inform 6, then you will probably find Ruby very easy
to use.


## Why JRuby?

JRuby provides access to a wealth of useful Java libraries, some of which I
found to be either necessary or at least extraordinarily facilitative to the
features needed for a multi-player server.

Primarily, the asynchronous-IO network driver Netty from JBoss is used for the
network communication of the game server.

Also, I wanted to use the JVM so that I wouldn't have to muck about too much
with low-level memory-management.  If I had concerned myself with such a
low-level detail, it would have significantly extended the duration of this
already lengthy development process.  I probably would have felt compelled to
attempt to replicate the original Z-Machine memory management system, which
would in turn have led me to attempt to support the entire Inform 6 language
and compilation paradigm.  Ruby would probably not have been a good choice for
that, or else I would have wound up trying to do some sort of Ruby extension
in C or Java.  I did take a close look at the Z-Machine Preservation Project,
and it could have been fun to attempt to get an entire Z-Machine working with
a network driver.  So who knows?  Maybe someone will come along and make that
a possibility, ideally in the form of some sort of plugin (support for which
would then have to be added to this project).  I see no reason why networking,
persistence, and support for in-game builder commands would be incompatibile
with a real Z-Machine.


## Why JBoss Netty?

JBoss Netty's features seemed compelling.  Its abstraction layer seemed
concise and documentation was ample.  Also there were already some decent
examples for netty-based telnet servers out there.  There seemed to be a lot
of floundering MUD projects out there trying to write their own network driver
so I felt it would be a waste of time.


## Why Ruby-Sequel?

The rcte-tree plugin that comes shipped with the sequel gem allowed for what
seemed to be a relatively straight-forward implementation of persistence of an
Inform-esque world-tree and other features.

The comprehensive knowledge of Sequel's components and functionality of its
creator, [Jeremy Evans](https://github.com/jeremyevans), and his apparently constant availability in the
Ruby-Sequel Talk Google forum has been crucial in the implementation of the
persistent world-tree data model.  Questions I had regarding Sequel were
nearly always immediately answered by Mr. Evans, and he deserves at least
acknowledgement and my eternal gratitude.

At first, I had given ActiveRecord a shot, but it failed when it came to
identity-mapping, and implementing a sufficiently consistent tree data-model
seemed to be nearly impossible.


## Why PostgreSQL?

At first I considered writing everything to files on disk in a directory tree,
like many other muds seem to do.  But it seems that lots of MUDs seem to have 
trouble with real-time persistence because of this approach, and things don't
always get saved when they are supposed to when bad things happen like system
crashes.  So I figured a nice open-source battle hardened relational database
would be sufficient and a good fit.  I wouldn't be opposed to having a
persistent plugin implemented for a variety of persistence options, like
file-system, MySQL, or non-relational database backends like MongoDB or Redis.

I also considered using Maglev to support implicit data persistence.  This
actually would have been pretty ideal, but Maglev servers are pricey.  Again,
perhaps this would be possible as an option somehow.  The idea of implicit
persistence was so attractive that I even experimented with getting JRuby to
store every variable in the system in Terracotta, like [Fabio Kung](http://fabiokung.com/2008/10/08/jruby-sharing-objects-across-multiple-runtimes-jmaglev/) had managed
to do, to some extent.  I really wish this had worked out.


## Data model

The data model is composed of objects, tags, links, and modules.

* An object may have an arbitrary number of key-value pair properties in
  addition to its standard fields like name, short_name, and description. The
  arbitrary values in the property hash are currently serialized using yaml,
  because that was done easiest with Ruby, but I think I may switch to JSON in
  order to take advantage of PostgreSQL's json data- type instead.  Note that
  property values may currently only be simple primitives.
* A tag is used to represent the boolean Inform Attribute.
* A link is used to complete the object property functionality by providing
  support for keyed first-class object members when simple primitives will not
  suffice.
* A module is used to apply modular sets of functionality to objects at
  runtime.


## Compromises

Some compromises were made in the port of the Inform 6 Parser.  The port is
initially intended to be used primarily to support multi-player games.  To
this end, I wrote a mud game in tandem with my porting efforts.  This game is
both a properietary product, as well as the baseline against which I tested
most of the ported features from the Inform 6 libraries.

The original parser makes extensive use of labels and jumps.  Ruby doesn't
support this.  Because of the complexity of the nested jumping, it seemed very
difficult to unwind.  So I had to hack rudimentary support for some shallow
jumping, and it is used in some places.  I don't like it at all.  Most
labeling and jumping has been replaced with standard invocations of new
functions, and exception handling where possible.

Arrays in Ruby are another significant departure from Inform 6.  This means
that some indexing of user command phrase strings may not be exactly the same
as it is in the original.  The buffer and parse data structures are prime
examples of this -- they have been completely replaced with a simple string
variable called `@input`, and an array called `@words`.

Custom parse routines are tricky.  There were several features which I felt
were necessities for a mud game for which I wasn't able to figure out how to
implement specialized tokens.  For instance, identifying logged-in players
during parsing.  Also, object ids, raw text, emote text, text queries which
would populate a special variable with a result, and time, were also features
for which I needed to add custom tokens in the ParseToken routine.  I would
prefer to be able to have these be custom parse routines, but I couldn't
figure out how to accomplish this with the standard Inform 6 mechanism for
supporting custom scope and parse routines.  So, I had to add the custom
parsing algorithms directly into the ParseToken routine for the particular
cases in the switch on the given token type.  This breaks my preference to
adhere to the original Inform 6 reference specification as closely as
possible.


## Roadmap

This is a list of todos for the Ruby Inform port and the multi-user server.
Some of these have already been completed.  Some are only partly completed.
Some may never be accomplished.

1. Fully implement all features of the original Inform Parser specification
   implementation.  [Mostly done.]

    A. Eliminate all bugs created during the porting process. [Mostly done.]

    B. Figure out how to allow game-designer overrides of scope and object
       name-parsing algorithms.  [Partly done.]

2. Multi-player games.  [Done.]

    A. Add an eventing system for multi-player games.  [Done.]

    B. Implement a pub-sub system for objects like player characters.  [Done.]

    C. Integrated client support for a stand-alone pub-sub system such as
       RabbitMQ for horizontal scalability.  [Michael Granger](https://github.com/ged)'s
       project [MUES](https://github.com/ged/mues) does this.  Ideally, this
       could also support horizontally scalable persistent eventing as well,
       but that might require some novel proc/lambda marshalling to support
       the inline-event block idiom I'm using to support easy-to-code behavior
       events.  Maybe something like [Ng Tze Yang](https://github.com/ngty)'s
       project [serializable_proc](https://github.com/ngty/serializable_proc)
       which is based on his other project, [sourcify](https://github.com/ngty/sourcify).
       But that'd be just insane if it actually worked.  The only down side of
       this I imagine at first might be that objects would have to explicitly
       subscribe to a particular channel upon entering or being placed in a
       particular location.

3. Full telnet protocol support.

    A. An inline color syntax system, too.

    B. Mud Server Status Protocol support.

4. Add a proper queueing system for the daemons and ad-hoc time-sensitive
   events.

5. Wrap information update publications in a queue per connection, so that new
   event info emissions do not interrupt each other and embed themselves
   within each other in the output.

6. Make persistence of the object tree optional (no database required).

7. Unit tests for the parser.

8. Behavioral specification tests for all of the standard grammars and verb
   library subroutines found in the reference Inform 6 Library implementation.

9. Support code-reloading during runtime.  This seems like it would be tricky
   given the way events are currently implemented.

10. Make parser optionally stand-alone and includable (no networking
    required).

    A. Support an entirely non-event-based, non-persistent, single-player,
       pure mode for Inform-style Ruby interactive-fiction story codez.

11. Create a side-by-side re-implementation of the Inform Parser which would
    optimize the memory management routines that seem to be mostly custom-
    designed to work around the absence of hash maps and modern memory
    management in the Inform language.  I already did some of this in a
    handful of places where I thought it would be prudent.  However, I feel
    this compromises the integrity of a faithful port of the Inform Parser.
    This would ideally be done in a fork, and then this modernized port would
    become the default version for mud distributions, and the more faithful
    version could be loaded as a plugin to be used for reference purposes or
    in a single-user mode.

12. Implement an irb console for invoking a stand-alone non-networked console
    for interacting with the code for the parser, object-tree, and other
    subsystem components a la the Sequel irb console.

13. Make the system fully compatible with all versions of Matz's Ruby
    Implementation, or at least CRuby 1.9.3.  This would mean no Java-based
    libraries, so the JDBC-Postgres driver, the JBoss Netty network driver,
    and the java.util.concurrent-based event system would all have to be
    replaced with something else.

14. It would be nice to have a plugin system for various backend persistence
    systems like alternative relational databases, or other non-relational
    database technologies.  Or maybe just abstract everything to consume
    RESTful data APIs.

15. Implement in-game support for Inform 7.  (Supporting inline Inform 7 code
    within Ruby source code via some sort of DSL implementation seems like it
    would be impossibly wonky or just impossible.) To me, this would be
    wonderful for networked multi-user games in terms of building the world
    and the programming of AI logic for non-player characters in-game.  This
    would be the ultimate holy-grail feature for this project, in my opinion.

16. Come up with an actual name for the binary executable and the project as a
    whole.  I only used 'zmud' because I wanted to reference the Z-Machine.
    Clearly this could be confused with Zugg's zMUD Client.  I didn't want to
    use an acronym either, like 'ripmus' for Ruby Inform Port and Multi-user
    Server.  Then again, maybe zmud is okay.  I've kinda grown to like it.


## Special thanks

I'd like to especially thank Mike Nerone, Jonathan Kelly, Jeremy Evans, Kevin
Bailey, Yoko Harada, Andrew Plotkin (zarf), and of course, Graham Nelson.
These folks were beyond imperative, and truly gracious with their time and
attention.

